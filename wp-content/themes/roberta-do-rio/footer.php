        <footer>
            <div class="wrap footer-content">
                <div class="logo-footer">
                    <a href="">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/svg/logo.svg" alt="">
                    </a>
                </div>
                <div>
                    <ul class="list-links">
                        <li><a href="">FAQ</a></li>
                        <li><a href="">Shipping</a></li>
                        <li><a href="">Return & Refund Policy</a></li>
                        <li><a href="">Privacy Policy</a></li>
                        <li><a href="">Contact Us</a></li>
                    </ul>
                </div>
                <div>
                    <h4 class="sub-title">Follow</h4>
                    <ul class="list-links">
                        <li><a href="">Facebook</a></li>
                        <li><a href="">Instagram</a></li>
                    </ul>
                </div>
                <div class="form-news">
                    <h4 class="sub-title">Join our newsletter</h4>
                    <div class="form-footer">
                        <form action="">
                            <input type="email" name="" id="" placeholder="enter your e-mail" required>
                            <button type="submit">Ok</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="wrap sub-content-footer">
                <p>made with <img src="<?php echo get_template_directory_uri(); ?>/images/heart.png" alt=""> in <span>Ipanema</span></p>
            </div>
        </footer>

        <section class="sub-footer">
            <div class="wrap">
                <div>
                    <p>Copyright 2020 © Roberta do Rio | All rights reserved</p>
                </div>
                <div>
                    <p>Rua Garcia d'Avila 149, sl - Ipanema - Rio de Janeiro / 21 3259 7437</p>
                </div>
            </div>
        </section>

         <div id="toTop">
            <span></span>
        </div>

        <div id="btn-book">
            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/book-an-appointment.png" alt=""></a>
        </div>

        <?php wp_footer(); ?>

        <script src="<?php echo get_template_directory_uri(); ?>/scripts/jquery-3.5.1.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/swiper-bundle.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/slider.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/photoswipe.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/photoswipe-ui-default.min.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/jquery.elevatezoom.js"></script>
        <script src="<?php echo get_template_directory_uri(); ?>/scripts/main.js"></script>
    </body>
</html>