<?php /* Template Name: Page Meet Roberta */ ?>
<?php get_header(); ?>
<?php the_post(); ?>	

<main class="container-int">
    <section class="meet-content">
        <div class="box-title-main">
            <span>meet</span>
            <h2>Roberta do Rio</h2>
        </div>

        <div class="main-text">
            <p>Roberta believes jewellery should <span>bring colour</span> and <span>poetry</span> to your world. She creates <span>playful pieces</span> with a <span>whimsical sensibility</span>, often celebrating <span>natural</span> and <span>figurative icons</span>; flowers, leaves, hands, eyes and masks all feature in her work.</p>
        </div>

        <div class="image-meet-roberta">
            <img src="<?php echo get_template_directory_uri(); ?>/images/meet-roberta.jpg" data-zoom-image="<?php echo get_template_directory_uri(); ?>/images/site-ilustracao.jpg" alt="" class="meet-roberta-elevate">
        </div>
    </section>

    <section class="bio-content">
        <div class="wrap bio-content__main">
            <div class="text-bio-content">
                <p>Her designs combine rich natural gemstones, many of which are native to her homeland, with high polish gold. </p>
                <p>Playing with translucent cut gems and hand carved opaque stones, these colourful jewels are the main protagonists in her creative process. </p>
                <p>Alongside these precious stones, talismans are a central feature in Roberta Do Rio’s designs. Bringing love, care, fertility and luck, these forms are a daily reminder of all that is important in our lives.. </p>
            </div>
            <img src="<?php echo get_template_directory_uri(); ?>/images/roberta-rio.jpg" alt="">
        </div>
        <div class="main-text">
            <p>Many of Roberta’s clients <span>collect her pieces</span>, adding <span>new chapters</span> to their story over time. Layering pendants and bracelets together, adding a new earring or ring to their collection to be used day to day or to celebrate a <span>special occasion</span>.</p>
        </div>
    </section>

    <section class="brand-story">
        <div class="wrap">
            <div class="box-title-main box-title-align">
                <span>brand</span>
                <h2>Story</h2>
            </div>
            <div class="text-columns">
                <p>Roberta do Rio discovered her passion for jewellery design after taking a goldsmithing course in 2010. Having studied in London and New York, she returned to Rio and began making custom pieces for herself. </p>
                <p>Enchanted by Roberta’s charming designs, friends soon began to commission pieces. Introducing their own friends and family, her audience quickly grew.</p>
                <p>As demand for her pieces increased, so did the need to build a business around her passion. In 2014 Roberta set up her brand, launching an appointment only atelier in Rio de Janeiro. This intimate space fostered the growth of her business, helping her to build a loyal clientele who began to visit regularly in search of a new discovery.
                </p>
                <p>In 2017, the Brand opened its first flagship store in the iconic neighbourhood of Ipanema to great acclaim. 2020 will see the international launch of the Brand, in close collaboration with the Valery Demure Showroom.</p>
            </div>
        </div>
        <div class="made-component">
            <p>made with <img src="<?php echo get_template_directory_uri(); ?>/images/svg/heart-on.svg" alt=""> in <span>Ipanema</span></p>
        </div>
    </section>
</main>

<?php get_footer(); ?>