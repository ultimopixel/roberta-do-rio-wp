<!doctype html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php wp_head(); ?>
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri(); ?>/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/site.webmanifest">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Baskervville:ital@0;1&family=Montserrat:ital,wght@0,300;0,400;0,600;1,300;1,600&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/swiper-bundle.min.css">
        <link href="<?php echo get_template_directory_uri(); ?>/css/photoswipe/photoswipe.min.css" rel="stylesheet" />
        <link href="<?php echo get_template_directory_uri(); ?>/css/photoswipe/default-skin.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
    </head>

    <body>
        <!-- modal -->
        <div class="subscribe-mmodal">
            <!-- Modal content -->
            <div class="subscribe-mmodal-content">
                <div class="inner-content-modal">
                    <div class="image-news">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/Collections_Fragmentos_news.jpg" alt="">
                    </div>
                    <div class="text-news">
                        <div class="main-text">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/svg/rr-logo-modal.svg" alt="">
                            <h3>Join our list</h3>
                            <p>Be the <span>first</span> to find out sum dolor sit amet, consectetur e <span>fringilla non</span>.</p>
                        </div>
                        <form action="">
                            <input type="text" placeholder="e-mail" required>
                            <div>
                                <button type="submit">
                                    be the first
                                    <span class="icon-arrow-thin-right arrow-btn"></span>
                                </button> 
                                
                            </div>
                        </form>
                    </div>
                    <span class="subscribe-mmodal-close">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/svg/close-modal.svg" alt="">
                    </span>
                </div>
            </div>
        </div>
        <!-- fim modal -->

        <nav class="main-nav main-nav-home">
            <div class="wrapper">
                <div class="logo">
                    <a href="index.html">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/svg/logo.svg" alt="" class="logo-main">
                        <img src="<?php echo get_template_directory_uri(); ?>/images/svg/roberta-rio-logo-scroll.svg" alt="" class="logo-scrolled">
                    </a>
                </div>
                <div class="nav-content">
                    <ul class="nav-links">
                        <li>
                            <a href="#" class="desktop-item menu-drop">Shop</a>
                            <input type="checkbox" id="showMega">
                            <label for="showMega" class="mobile-item">Shop</label>
                            <div class="mega-box">
                                <div class="content">
                                    <div class="row box-jleft">
                                        <h3>Joias</h3>
                                        <ul class="mega-links">
                                            <li><a href="collection.html" data-filename="bloom-01.jpg">brincos</a></li>
                                            <li><a href="collection.html" data-filename="bloom-02.jpg">pingente</a></li>
                                            <li><a href="collection.html" data-filename="bloom-03.jpg">aneis</a></li>
                                            <li><a href="collection.html" data-filename="bloom-04.jpg">alianças</a></li>
                                            <li><a href="collection.html" data-filename="bloom-01.jpg">colares</a></li>
                                            <li><a href="collection.html" data-filename="bloom-02.jpg">one of a kind</a></li>
                                        </ul>
                                    </div>
                                    <div class="row">
                                        <h3>Coleções</h3>
                                        <ul class="mega-links">
                                            <li><a href="#" data-filename="bloom-01.jpg">marrocos</a></li>
                                            <li><a href="#" data-filename="bloom-03.jpg">fragmentos</a></li>
                                            <li><a href="#" data-filename="bloom-02.jpg">origens</a></li>
                                            <li><a href="#" data-filename="bloom-01.jpg">fire spell</a></li>
                                            <li><a href="#" data-filename="bloom-03.jpg">bloom</a></li>
                                            <li><a href="#" data-filename="bloom-02.jpg">jardim particular</a></li>
                                            <li><a href="#" data-filename="bloom-01.jpg">power</a></li>
                                            <li><a href="#" data-filename="bloom-02.jpg">encontros</a></li>
                                            <li><a href="#" data-filename="bloom-02.jpg">solei</a></li>
                                            <li><a href="#" data-filename="bloom-03.jpg">amuletos</a></li>
                                        </ul>
                                    </div>
                                    <div class="row image-box">
                                        <!-- imagem que será alterada no hover dos itens acima, o arquivo de data-filename irá substituir a img abaixo -->
                                        <img src="<?php echo get_template_directory_uri(); ?>/images/slides/slide-htr.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <input type="radio" name="slider" id="menu-btn-inst">
                    <input type="radio" name="slider" id="close-btn-inst">

                    <ul class="nav-links-inst">
                        <label for="close-btn-inst" class="btn close-btn-inst"></label>
                        <div class="nav-itens-mobile">
                            <li>
                                <a href="#" class="desktop-item menu-drop">
                                    <label for="showMegaMobile" class="mobile-item">Shop</label>
                                </a>
                                <input type="checkbox" id="showMegaMobile">
                                <!-- clone .mega-box, exibido apenas na versão mobile -->
                                <div class="mega-box">
                                    <div class="content">
                                        <div class="row box-jleft">
                                            <h3>Joias</h3>
                                            <ul class="mega-links">
                                                <li><a href="collection.html" data-filename="bloom-01.jpg">brincos</a></li>
                                                <li><a href="collection.html" data-filename="bloom-02.jpg">pingente</a></li>
                                                <li><a href="collection.html" data-filename="bloom-03.jpg">aneis</a></li>
                                                <li><a href="collection.html" data-filename="bloom-04.jpg">alianças</a></li>
                                                <li><a href="collection.html" data-filename="bloom-01.jpg">colares</a></li>
                                                <li><a href="collection.html" data-filename="bloom-02.jpg">one of a kind</a></li>
                                            </ul>
                                        </div>
                                        <div class="row">
                                            <h3>Coleções</h3>
                                            <ul class="mega-links">
                                                <li><a href="#" data-filename="bloom-01.jpg">marrocos</a></li>
                                                <li><a href="#" data-filename="bloom-03.jpg">fragmentos</a></li>
                                                <li><a href="#" data-filename="bloom-02.jpg">origens</a></li>
                                                <li><a href="#" data-filename="bloom-01.jpg">fire spell</a></li>
                                                <li><a href="#" data-filename="bloom-03.jpg">bloom</a></li>
                                                <li><a href="#" data-filename="bloom-02.jpg">jardim particular</a></li>
                                                <li><a href="#" data-filename="bloom-01.jpg">power</a></li>
                                                <li><a href="#" data-filename="bloom-02.jpg">encontros</a></li>
                                                <li><a href="#" data-filename="bloom-02.jpg">solei</a></li>
                                                <li><a href="#" data-filename="bloom-03.jpg">amuletos</a></li>
                                            </ul>
                                        </div>
                                        <div class="row image-box">
                                            <img src="<?php echo get_template_directory_uri(); ?>/images/slides/slide-htr.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </div>
                        <li><a href="meet-roberta.html">Meet Roberta</a></li>
                        <li>
                            <a href="#" class="item-menu-drop">Collections</a>
                            <ul class="sub-menu-drop">
                                <li><a href="fragmentos.html">marrocos</a></li>
                                <li><a href="fragmentos.html">fragmentos</a></li>
                                <li><a href="fragmentos.html">origens</a></li>
                                <li><a href="fragmentos.html">fire spell</a></li>
                                <li><a href="fragmentos.html">bloom</a></li>
                                <li><a href="fragmentos.html">jardim particular</a></li>
                                <li><a href="fragmentos.html">power</a></li>
                                <li><a href="fragmentos.html">encontros</a></li>
                                <li><a href="fragmentos.html">solei</a></li>
                                <li><a href="fragmentos.html">amuletos</a></li>
                            </ul>
                        </li>
                        <li><a href="store.html">The Store</a></li>
                        <li><a href="press.html">Press</a></li>
                        <li><a href="contact.html">Contact</a></li>
                    </ul>
                
                    <div class="icons-nav">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/svg/sacola.svg" alt=""></a>
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/svg/perfil.svg" alt=""></a>
                        <a href="#" class="hamburguer-mobile">
                            <label for="menu-btn-inst">
                                <img src="<?php echo get_template_directory_uri(); ?>/images/svg/menu-hamburguer-in-white.svg" alt="">
                            </label>
                        </a>
                    </div>
                </div> 
            </div>
        </nav>