<?php /* Template Name: Page Contact */ ?>
<?php get_header(); ?>
<?php the_post(); ?>	

<main class="container-int container-contact">
    <section class="store-content">
        <div class="box-title-main">
            <span>get in</span>
            <h2>touch</h2>
        </div>

        <div class="main-text">
            <p>For <span>inquiries</span> please <span>contact us</span> at</p>
            <a href="mailto:comercial@robertadorio.com" class="link-contact">comercial@robertadorio.com</a>

            <p>or <span>message</span> us on</p>
            <a href="https://wa.me/552109435344" class="number-phone link-contact">+55 21 09435-344</a>
        </div>

        <div class="wrap info-contact">
            <div class="row">
                <div>
                    <h4>Office</h4>
                    <p>Ipanema</p>
                    <p>Rio de Janeiro – RJ</p>
                    <p>T.: 21 2146 0900</p>
                    <a href="mailto:comercial@robertadorio.com">comercial@robertadorio.com</a>
                </div>

                <div>
                    <h4>Store</h4>
                    <p>Rua Garcia d'Avila, 149  </p>
                    <p>Ipanema, Rio de Janeiro </p>
                    <p>- RJ - Brazil</p>
                </div>
            </div>

            <div class="row">
                <div>
                    <h4>Press</h4>
                    <p>Multifato</p>
                    <p>Ipanema / RJ</p>
                    <p>T.: 21 2223 1258 | 21 3874 0214</p>
                </div>

                <div class="links-socials">
                    <h4>Social</h4>
                    <p><a href="https://www.instagram.com/robertadoriojewelry/" target="_blank">Instagram</a></p>
                    <p><a href="https://www.facebook.com/robertadoriojewelry/" target="_blank">Facebook</a></p>
                </div>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>