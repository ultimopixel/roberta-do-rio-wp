<?php get_header(); ?>
<?php the_post(); ?>	

<main class="container-int container-frag container-press">
    <section class="grid-image grid-image--roberta">

        <div class="header-press">
            <div class="date-press">set 2020</div>
            <div class="title-press">Caderno Ela</div>
            <div class="image-press">
                <img src="<?php echo get_template_directory_uri(); ?>/images/press_interna_header.jpg" alt="">
            </div>
        </div>

        <div class="wrap container-grid">
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/press_interna_02.jpg" alt="">
            </div>
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/press_interna_03.jpg" alt="">
            </div>
        </div>
    </section>

    <?php echo get_template_part('blocks/block-slider-products'); ?>
    
</main>

<?php get_footer(); ?>