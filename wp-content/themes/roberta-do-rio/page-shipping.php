<?php /* Template Name: Page Shipping */ ?>
<?php get_header(); ?>
<?php the_post(); ?>

<main class="container-int container-help">
    <section class="wrap">
        <div class="content-help">
            <div class="list-links-help">
                <ul>
                    <li><a href="">Packaging</a></li>
                    <li><a href="">FAQ</a></li>
                    <li><a href="">Shipping</a></li>
                    <li><a href="">Terms & Conditions</a></li>
                    <li><a href="">Return & Refund Policy</a></li>
                    <li><a href="">Privacy Policy</a></li>
                    <li><a href="">Cookie Policy</a></li>
                </ul>
            </div>

            <div class="text-content-help">
                <h2>Shipping</h2>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum doloremque odit laboriosam quo, suscipit soluta maiores at repellendus, temporibus corporis libero quam eos. Laudantium quae ratione vel qui aliquid atque?</p>

                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quis totam adipisci nesciunt laboriosam possimus exercitationem natus veniam nemo voluptatum, est, fugit nam eius neque reprehenderit molestiae et corporis mollitia.</p>

                <h3>More Shipping</h3>
                <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Harum doloremque odit laboriosam quo, suscipit soluta maiores at repellendus, temporibus corporis libero quam eos. Laudantium quae ratione vel qui aliquid atque?</p>

                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ad quis totam adipisci nesciunt laboriosam possimus exercitationem natus veniam nemo voluptatum, est, fugit nam eius neque reprehenderit molestiae et corporis mollitia.</p>

                <ul>
                    <li>item1</li>
                    <li>item2</li>
                    <li>item3</li>
                    <li>item4</li>
                    <li>item5</li>
                </ul>
            </div>
        </div>
    </section>

    <div class="made-component made-component-help">
        <p>made with <img src="<?php echo get_template_directory_uri(); ?>/images/svg/heart-on.svg" alt=""> in <span>Ipanema</span></p>
    </div>
</main>

<?php get_footer(); ?>