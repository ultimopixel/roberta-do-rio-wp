<?php /* Template Name: Page Size Chart */ ?>
<?php get_header(); ?>
<?php the_post(); ?>

<main class="container-int">
    <section class="store-content">
        <div class="box-title-main">
            <span>size</span>
            <h2>chart</h2>
        </div>

        <div class="wrap">
            <table class="table-main">
                <thead>
                    <tr>
                        <th>INTERNAL DIAMETER IN MM</th>
                        <th>US</th>
                        <th>UK</th>
                        <th>EUROPE</th>
                        <th>BRAZIL</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>15.1</td>
                        <td>4.25</td>
                        <td>1</td>
                        <td>46.5</td>
                        <td>10</td>
                    </tr>
                    <tr>
                        <td>15.4</td>
                        <td>4.75</td>
                        <td>I 1/2</td>
                        <td>48.5</td>
                        <td>11</td>
                    </tr>
                    <tr>
                        <td>15.75</td>
                        <td>5</td>
                        <td>J 1/2</td>
                        <td>49</td>
                        <td>12</td>
                    </tr>
                    <tr>
                        <td>16.1</td>
                        <td>5.25</td>
                        <td>K 1/2</td>
                        <td>50.5</td>
                        <td>13</td>
                    </tr>
                    <tr>
                        <td>16.3</td>
                        <td>5.75</td>
                        <td>L</td>
                        <td>51</td>
                        <td>13.5</td>
                    </tr>
                    <tr>
                        <td>16.4</td>
                        <td></td>
                        <td></td>
                        <td>51.5</td>
                        <td>14</td>
                    </tr>
                    <tr>
                        <td>16.5</td>
                        <td>6</td>
                        <td>L 1/2</td>
                        <td>52</td>
                        <td>14</td>
                    </tr>
                    <tr>
                        <td>16.75</td>
                        <td></td>
                        <td>M</td>
                        <td></td>
                        <td>15</td>
                    </tr>
                    <tr>
                        <td>16.9</td>
                        <td>6.5</td>
                        <td>M 1/2</td>
                        <td>53</td>
                        <td>15.5</td>
                    </tr>
                    <tr>
                        <td>17.15</td>
                        <td>6.75</td>
                        <td>N</td>
                        <td>53.5</td>
                        <td>16</td>
                    </tr>
                    <tr>
                        <td>17.3</td>
                        <td>7</td>
                        <td>N 1/2</td>
                        <td>54</td>
                        <td>16.5</td>
                    </tr>
                    <tr>
                        <td>17.5</td>
                        <td>7.25</td>
                        <td>O</td>
                        <td>55</td>
                        <td>17</td>
                    </tr>
                    <tr>
                        <td>17.9</td>
                        <td></td>
                        <td>P</td>
                        <td>56</td>
                        <td>18</td>
                    </tr>
                    <tr>
                        <td>18.2</td>
                        <td>8</td>
                        <td>P 1/2</td>
                        <td>57</td>
                        <td>19</td>
                    </tr>
                    <tr>
                        <td>18.5</td>
                        <td>8.5</td>
                        <td>Q 1/2</td>
                        <td>58</td>
                        <td>20</td>
                    </tr>
                    <tr>
                        <td>18.8</td>
                        <td></td>
                        <td>R</td>
                        <td>59</td>
                        <td>21</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>9</td>
                        <td>R 1/2</td>
                        <td>59.5</td>
                        <td>21.5</td>
                    </tr>
                    <tr>
                        <td>19.2</td>
                        <td></td>
                        <td>S</td>
                        <td>60</td>
                        <td>22</td>
                    </tr>
                    <tr>
                        <td>19.4</td>
                        <td>9.5</td>
                        <td>S 1/2</td>
                        <td>61</td>
                        <td>22.5</td>
                    </tr>
                    <tr>
                        <td>19.6</td>
                        <td></td>
                        <td>T</td>
                        <td></td>
                        <td>23</td>
                    </tr>
                    <tr>
                        <td>19.8</td>
                        <td>10</td>
                        <td>T 1/2</td>
                        <td>62</td>
                        <td>24</td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td></td>
                        <td>U</td>
                        <td>62.5</td>
                        <td>24.5</td>
                    </tr>
                    <tr>
                        <td>20.2</td>
                        <td>10.5</td>
                        <td>U 1/2</td>
                        <td>63</td>
                        <td>25</td>
                    </tr>
                    <tr>
                        <td>20.4</td>
                        <td></td>
                        <td>V</td>
                        <td>64</td>
                        <td>25.5</td>
                    </tr>
                    <tr>
                        <td>20.6</td>
                        <td>11</td>
                        <td>V 1/2</td>
                        <td>64.5</td>
                        <td>26</td>
                    </tr>
                    <tr>
                        <td>20.9</td>
                        <td></td>
                        <td></td>
                        <td>65.5</td>
                        <td>27</td>
                    </tr>
                </tbody>
            </table>
        </div>
        

        <div class="main-text">
            <p>Roberta believes jewellery should <span>bring colour</span> and <span>poetry</span> to your world. She creates <span>playful pieces</span> with a <span>whimsical sensibility</span>, often celebrating <span>natural</span> and <span>figurative icons</span>; flowers, leaves, hands, eyes and masks all feature in her work.</p>
        </div>

    </section>
</main>

<?php get_footer(); ?>