<?php /* Template Name: Page Store */ ?>
<?php get_header(); ?>
<?php the_post(); ?>

<main class="container-int">
    <section class="store-content">
        <div class="box-title-main">
            <span>the</span>
            <h2>store</h2>
        </div>

        <div class="main-text">
            <p>Roberta believes jewellery should <span>bring colour</span> and <span>poetry</span> to your world. She creates <span>playful pieces</span> with a <span>whimsical sensibility</span>, often celebrating <span>natural</span> and <span>figurative icons</span>; flowers, leaves, hands, eyes and masks all feature in her work.</p>
        </div>

        <div class="image-store">
            <img src="<?php echo get_template_directory_uri(); ?>/images/store.jpg" alt="">
        </div>

        <div class="main-text sub__main-text text-lh">
            <p>Unindo o <span>clássico e o contemporâneo</span>, Roberta do Rio transforma pedras preciosas e nobres metais em <span>peças desejo</span>, <span>únicas</span> e <span>feitas à mão</span>, todas fabricadas em seu ateliê próprio.</p>
        </div>

    </section>

    <section class="sub__store-content">
        <div class="wrap image-grid">
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/store-images_01.jpg" alt="">
            </div>
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/store-images_02.jpg" alt="">
            </div>
            <div>
                <img src="<?php echo get_template_directory_uri(); ?>/images/store-images_03.jpg" alt="">
            </div>
        </div>

        <div class="footer-store">
            <p class="store-address">Rua Garcia d'Avila, 149</p>
            <p class="store-city">Ipanema, Rio de Janeiro - RJ</p>

            <a href="https://goo.gl/maps/hiakt6jFYDuYRWTG9" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/map-roberta-rio.png" data-zoom-image="<?php echo get_template_directory_uri(); ?>/images/mapa-big.jpg" alt="" class="meet-roberta-elevate"></a>
        </div>
    </section>
</main>

<?php get_footer(); ?>