<section class="slider-produtos">
    <div class="swiper-container">
        <!-- texto que fica no carrossel de produtos, esse é exibido no mobile -->
        <div class="swiper-txt swiper-txt-mobile">
            <span>roberta's</span>
            <h2>Picks</h2>
        </div>
        
        <div class="swiper-wrapper">
            <!-- texto que fica no carrossel de produtos, exibido em telas maiores -->
            <div class="swiper-txt swiper-slide">
                <span>roberta's</span>
                <h2>Picks</h2>
            </div>

            <div class="swiper-slide">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/grid-roberta/roberta-02.jpg" alt="">
                </a>
                <div class="info-produto">
                    <p>Brinco Flower</p>
                    <i class="heart-swiper"></i>
                </div>
            </div>

            <div class="swiper-slide">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/grid-bloom/bloom-02.jpg" alt="">
                </a>
                <div class="info-produto">
                    <p>Brinco Flower</p>
                    <i class="heart-swiper"></i>
                </div>
            </div>

            <div class="swiper-slide">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/grid-origens/origens-04.jpg" alt="">
                </a>
                <div class="info-produto">
                    <p>Brinco Flower</p>
                    <i class="heart-swiper"></i>
                </div>
            </div>

            <div class="swiper-slide">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/produto-example.jpg" alt="">
                </a>
                <div class="info-produto">
                    <p>Brinco Flower</p>
                    <i class="heart-swiper"></i>
                </div>
            </div>

            <div class="swiper-slide">
                <a href="#">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/produto-example.jpg" alt="">
                </a>
                <div class="info-produto">
                    <p>Brinco Flower</p>
                    <i class="heart-swiper"></i>
                </div>
            </div>

        </div>
        <div class="swiper-pagination"></div>
    </div>
</section>