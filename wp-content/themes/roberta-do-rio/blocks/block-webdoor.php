<?php

$webdoor_style = ($block['data']['webdoor_style'] == 'style_2') ? 'htr' : 'default'; 

$i=0;
while($i < $block['data']['webdoor_items']){
    $webdoor_items[] = [        
        'title' => $block['data']['webdoor_items_'.$i.'_webdoor_items_item_title'],
        'subtitle' => $block['data']['webdoor_items_'.$i.'_webdoor_items_item_subtitle'],
        'button_text' => $block['data']['webdoor_items_'.$i.'_webdoor_items_item_button_text'],
        'button_link' => get_the_permalink($block['data']['webdoor_items_'.$i.'_webdoor_items_item_button_link']),
        'type' => $block['data']['webdoor_items_'.$i.'_webdoor_items_item_type'],
        'image' => wp_get_attachment_url($block['data']['webdoor_items_'.$i.'_webdoor_items_item_image']),
        'video' => wp_get_attachment_url($block['data']['webdoor_items_'.$i.'_webdoor_items_item_video']),
        'position' => $block['data']['webdoor_items_'.$i.'_webdoor_items_item_position'],
        'bg_color' => $block['data']['webdoor_items_'.$i.'_webdoor_items_item_bg_color'],
        'bg_image' => wp_get_attachment_url($block['data']['webdoor_items_'.$i.'_webdoor_items_item_bg_image']),
    ];
    $i++;
}
?>

<div class="slider-container">
    <div class="slider-control left inactive"></div>
    <div class="slider-control right"></div>
    <ul class="slider-pagi"></ul>

    <!-- Duas versões para o slider -> slider-default ou slider-htr -->
    <div class="slider slider-<?php echo $webdoor_style; ?>"> <!-- <div class="slider slider-htr"> -->

        <?php foreach($webdoor_items as $key => $item){ ?>

            <?php if($block['data']['webdoor_style'] == 'style_1' || ($block['data']['webdoor_style'] == 'style_2' && $item['position'] == 'image_first')){ ?>
                <div class="slide slide-<?php echo $key; ?> <?php echo ($key == 0) ? 'active' : ''; ?>">
                    <!-- as imagens full estão inseridas via background-image na classe slide__bg -->
                    <?php $bg = 'style="background-image:url('.$item['bg_image'].');"'; ?>
                    <div class="slide__bg" <?php echo $bg; ?>></div> 
                    <div class="slide__content">

                        <?php if(strlen($item['video']) > 0){ ?>
                            <video class="bg-video__content" autoplay loop muted playsinline poster="">
                                <source src="<?php echo $item['video']; ?>" type="video/mp4">
                                Desculpe, seu browser não suporta esse formato de vídeo!
                            </video>
                        <?php } ?>

                        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
                            <!-- área da segunda versão do slide, para mudar a cor alterar o fill, no momento está via CSS, mas se for dinâmico, teremos que colocar o fill inline -->
                            <path class="slide__overlay-path" d="M0,0 350,0 350,405 0,405" fill="<?php echo $item['bg_color']; ?>" />
                        </svg>
                        <div class="slide__text discover-collection">
                            <div class="img-slide">
                                <!-- essa imagem é da segunda versão do slide -->
                                <img src="<?php echo $item['image']; ?> " alt="">
                            </div>
                            <div class="txt-slide">
                                <h2><?php echo $item['title']; ?></h2>
                                <p><?php echo $item['subtitle']; ?></p>
                                <a href="<?php echo $item['button_link']; ?> ">
                                    <?php echo $item['button_text']; ?> 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php }else{ ?>
                <div class="slide slide-<?php echo $key; ?>">
                    <?php $bg = 'style="background-image:url('.$item['bg_image'].');"'; ?>
                    <div class="slide__bg" <?php echo $bg; ?>></div> 
                    <div class="slide__content">
                        <svg class="slide__overlay" viewBox="0 0 720 405" preserveAspectRatio="xMaxYMax slice">
                            <path class="slide__overlay-path" d="M0,0 385,0 385,405 0,405" />
                        </svg>
                        <div class="slide__text discover-collection">
                            <div class="img-slide">
                                <video class="" autoplay loop muted playsinline poster="">
                                    <source src="<?php echo $item['video']; ?>" type="video/mp4">
                                    Desculpe, seu browser não suporta esse formato de vídeo!
                                </video>
                            </div>
                            <div class="txt-slide">
                                <h2><?php echo $item['title']; ?></h2>
                                <p><?php echo $item['subtitle']; ?></p>
                                <a href="<?php echo $item['button_link']; ?> ">
                                    <?php echo $item['button_text']; ?> 
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>

        <?php } ?>

    </div>
</div>