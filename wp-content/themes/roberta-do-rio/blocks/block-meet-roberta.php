<section class="meet-roberta">
    <div class="wrap">
        <div class="row-container">
            <div class="box-text">
                <span><?php echo $block['data']['block_meet_roberta_title']; ?></span>
                <h2><?php echo $block['data']['block_meet_roberta_subtitle']; ?></h2>
                <p>
                    <?php echo $block['data']['block_meet_roberta_text_1']; ?>
                </p>
            </div>
            <div class="image-main">
                <img src="<?php echo wp_get_attachment_url($block['data']['block_meet_roberta_image_1']); ?>" />
            </div>
        </div>

        <div class="row-container row-reverse">
            <div class="box-text">
                <p>
                    <?php echo $block['data']['block_meet_roberta_text_2']; ?>
                    <a href="<?php echo get_the_permalink($block['data']['block_meet_roberta_link']); ?>" class="simple-link">
                        <?php echo $block['data']['block_meet_roberta_link_text']; ?>
                    </a>
                </p>
            </div>
            <div class="image-main">
                <div class="img-hei">
                    <img src="<?php echo wp_get_attachment_url($block['data']['block_meet_roberta_image_2']); ?>" />
                </div>
            </div>
        </div>
    </div>
</section>