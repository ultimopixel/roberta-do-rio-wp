<section class="discover-collection bloom-collection" style="background-image: url(<?php echo wp_get_attachment_url($block['data']['discover_collection_bg']); ?>); ">
    <h2><?php echo $block['data']['discover_collection_title']; ?></h2>
    <p><?php echo $block['data']['discover_collection_subtitle']; ?></p>
    <a href="<?php echo get_the_permalink(get_field('discover_collection_link')); ?>">
        <?php echo $block['data']['discover_collection_link_text']; ?>
    </a>
</section>