<section class="grid-image" style="background-color: <?php echo $block['data']['grid_image_bg_color']; ?>">
    <div class="main-text">
        <?php if(strlen($block['data']['grid_image_main_image'])){ ?>
            <img src="<?php echo wp_get_attachment_url($block['data']['grid_image_main_image']); ?>" />
        <?php } ?>
        <?php if(strlen($block['data']['grid_image_main_text'])){ ?>
            <p>
                <?php echo $block['data']['grid_image_main_text']; ?>
            </p>
        <?php } ?>        
    </div>
    <div class="wrap container-grid">
        <?php
        foreach($block['data'] as $field => $value){ 
            if(strpos($field, 'grid_image_content_') !== false && strpos($value, 'field_') === false){
                $key = str_replace('grid_image_content_','', $field);
                $key = explode('_', $key);
                $grid_image_content[$key[0]][$key[1]] = $value;
            }
        }
        ?>
        <?php foreach($grid_image_content as $content){ ?>
            <?php if($content['type'] == 'image'){ ?>
                <div>
                    <a href="">
                        <img src="<?php echo wp_get_attachment_url($content['image']); ?>" alt="">
                    </a>
                </div>
            <?php } ?>
            <?php if($content['type'] == 'full-width-image'){ ?>
                <div class="full-image-grid">
                    <a href="">
                        <img src="<?php echo wp_get_attachment_url($content['image']); ?>" alt="">
                    </a>
                </div>
            <?php } ?>  
            <?php if($content['type'] == 'text'){ ?>
                <div class="text-grid">
                    <div class="main-text">
                        <p><?php echo $content['text']; ?></p>
                    </div>
                </div>
            <?php } ?>                        
        <?php } ?>
    </div>
    <div class="btn btn-shop btn-rr">
        <a href="<?php echo get_the_permalink($block['data']['grid_image_button_link']); ?>">
            <?php echo $block['data']['grid_image_button_text']; ?>
            <span class="icon-arrow-thin-right arrow-btn"></span>
        </a>
    </div>
</section>