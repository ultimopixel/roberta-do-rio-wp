<?php  

add_theme_support( 'post-thumbnails' );
add_theme_support( 'menus' );
add_theme_support( 'widgets' );	
add_theme_support( 'editor' );	

add_action( 'after_setup_theme', 'up_setup' );
function up_setup(){
	// add_image_size( 'medium', 655, 425, true );	
}

if(function_exists('acf_register_block_type')){
	add_action('acf/init', 'register_acf_block_types');
}
function register_acf_block_types(){

	acf_register_block_type( array(
		'name'			=> 'webdoor',
		'title'			=> __( 'Webdoor'),
		'render_template'	=> 'blocks/block-webdoor.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'webdoor' )
	));	

	acf_register_block_type( array(
		'name'			=> 'grid-image',
		'title'			=> __( 'Grid Image'),
		'render_template'	=> 'blocks/block-grid-image.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'grid-image' )
	));		

	acf_register_block_type( array(
		'name'			=> 'discover-collection',
		'title'			=> __( 'Discover Collection'),
		'render_template'	=> 'blocks/block-discover-collection.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'discover-collection' )
	));	

	acf_register_block_type( array(
		'name'			=> 'meet-roberta',
		'title'			=> __( 'Meet Roberta'),
		'render_template'	=> 'blocks/block-meet-roberta.php',		
		'icon'			=> 'layout',
		'mode'			=> 'edit',
		'keywords'		=> array( 'section', 'meet-roberta' )
	));		

}

function get_block_data($post, $block){
	
}