<?php /* Template Name: Page Press */ ?>
<?php get_header(); ?>
<?php the_post(); ?>	

<main class="container-int container-frag press-list">
    <section class="grid-image">

        <div class="box-title-main">
            <span>media</span>
            <h2>Press</h2>
        </div>

        <div class="wrap container-grid">
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_03.jpg" alt="">
                </a>
            </div>
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_06.jpg" alt="">
                </a>
            </div>

            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_10.jpg" alt="">
                </a>
            </div>
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_13.jpg" alt="">
                </a>
            </div>
        </div>

        <div class="wrap half-image-grid">
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_18.jpg" alt="">
                </a>
            </div>
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_20.jpg" alt="">
                </a>
            </div>
        </div>

        <div class="wrap container-grid">
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_24.jpg" alt="">
                </a>
            </div>
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_27.jpg" alt="">
                </a>
            </div>

            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_29.jpg" alt="">
                </a>
            </div>
            <div>
                <a href="press_interna.html">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/press/Press_32.jpg" alt="">
                </a>
            </div>
        </div>
    </section>
</main>

<?php get_footer(); ?>